default: image

image:
	docker pull neubauergroup/centos-python3:3.8.10
	docker build . \
		--file Dockerfile \
		--tag test/centos-root-base:debug-local
